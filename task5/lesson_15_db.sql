-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 13 2017 г., 16:35
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `book_base`
--

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `href` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `type`, `name`, `href`) VALUES
(1, 'pdf', 'Люк Веллинг, \"Разработка веб-приложений с помощью PHP\"', '/files/webappsdev.pdf'),
(2, 'doc', 'Кевин Янк, \"PHP и MySQL. От новичка к профессионалу\"', '/files/phpmysql.doc'),
(3, 'pdf', 'Бретт Маклафилин, \"PHP и MySQL. Исчерпывающее руководство\"', '/files/phpandmysqlall.pdf'),
(4, 'pdf', 'Павел Савинов, \"PHP:Правильный путь\"', '/files/truephp.pdf'),
(5, 'txt', 'Роберт Никсон, \"Создаем динамические веб-сайты с помощью PHP, MySQL и JavaScript\"', '/files/dynamicweb.txt'),
(6, 'txt', 'Мэт Зандстра \"PHP: объекты, шаблоны и методики программирования\"', '/files/phpmetod.txt');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
