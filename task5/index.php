<?php

//Урок 13: Объектно-ориентированное программирование #1

/*Создать класс Car. 
1) Добавить классу 3 свойства на свое усмотрение (например: количество дверей, стоимость, ...). Одному из свойств присвоить значение по умолчанию. 
2) Создать 4 объекта класса Car. 
3) Для двух объектов задать значения свойств, используя обращение к свойству (например, $car1->price). 
4) Для двух оставшихся задать свойства используя конструктор (написать констуктор для инициализации объекта). 
5) Написать функцию fuelСonsumption() для расчета количества топлива, затраченного на поездку на заданную дистанцию. Используйте свойство объекта $fuel (расход топлива на 100 км). 
6) Добавить в класс 3 числовых константы (со значениями, например, 2,5,12). Распечатать значения констант в коде программы (вне класса). 
7) Добавить статический метод getMaxConstant(). Этот метод находит наибольшую из констант класса и возвращает её значение. Не забывайте, что статические методы принадлежат классу, и вызываются из контекста класса.*/

    echo "<h1>"."Lesson 13"."</h1>";

    class Car 
    {
        const TWO = 2;
        const FIVE = 5;
        const TWELVE = 12;

        var $doorsCount = 4;
        var $price;
        var $fuel;

        function __construct($doorsCount, $price, $fuel) 
        {
            $this->doorsCount = $doorsCount;
            $this->price = $price;
            $this->fuel = $fuel;
        }

        public function fuelСonsumption($way)
        {
            return ($way / 100) * $fuel;
        }

        public static function getConstant()
        {
            $a = new ReflectionClass(__CLASS__);
            $constants = $a->getConstants();

            foreach( $constants as $key => $value ) {
                echo "{$key} : {$value}<br/>";
            }
        }

        public static function getMaxConstant()
        {
            $a = new ReflectionClass(__CLASS__);
            $constants = $a->getConstants();
            $max = PHP_INT_MIN;

            foreach( $constants as $key => $value ) {
                if ( $max < $value ) {
                    $max = $value;
                }
            }

            return $max;
        }

    }

    $car1 = new Car(4, 4000, 7);
    $car2 = new Car(2, 2000, 6);

    $car1->fuel = 10;
    $car2->fuel = 15;

    $car3 = new Car(4, 5000, 12);
    $car4 = new Car(4, 7500, 8);

    Car::getConstant();

    $max = Car::getMaxConstant();
    echo "Max const = {$max}";

//Урок 14: Объектно-ориентированное программирование #2

/*1) Создать класс Figure - плоская геометрическая фигура. У этого класса есть такие свойства: площадь, цвет. И константа: количество сторон. 
2) Для класса Figure написать метод infoAbout(). Этот метод возвращает сообщение: "Это геометрическая фигура!". 
3) От класса Figure унаследовать классы: Rectangle, Triangle, Square. 
4) Добавить для Rectangle приватные свойства a,b - длины сторон. 
5) Добавить для Square приватное свойство a - длина стороны. 
6) Добавить для Triangle приватные свойства a, b, c - длины сторон. 
7) Для каждого из классов Rectangle, Triangle, Square определить значение константы: количество сторон. Например, для квадрата: const SIDES_COUNT = 4; 
8) Создать конструкторы для классов Rectangle, Triangle, Square для инициализации значений длин сторон. 
9) Для каждого из классов Rectangle, Triangle, Square реализовать метод getArea() - подсчет площади. Методы возвращают значение площади. 
10) Для каждого из классов Rectangle, Triangle, Square переопределить метод infoAbout() так, что б он возвращал строку такого содержания: (пример для квадрата): "Это класс квадрата. У него 4 стороны". Аналогично для других классов. 
11) Сделать методы infoAbout() финальными. 
12) Для каждого класса Rectangle, Triangle, Square создать по 2 объекта (с передачей значений длин сторон в конструктор). 
13) Вызвать для всех объектов методы getArea(), вывести результаты.*/

    echo "<h1>"."Lesson 14"."</h1>";

    class Figure 
    {

        const SIDES_COUNT = 4;

        public $area = 0;
        public $color;

        public function infoAbout() 
        {
            return "Это геометрическая фигура!";
        }

    }

    class Rectangle extends Figure
    {     

        const SIDES_COUNT = 4;
        
        private $a;
        private $b;
           
        function __construct($a, $b) 
        { 
            $this->a = $a;
            $this->b = $b;       
        } 
        
        public function getArea()
        {
            $this->area = $this->a * $this->b;
            return $this->area;
        }  
        
        public final function infoAbout()
        {
            return "Это прямоугольник. У него 4 стороны";
        }

    }

    class Square extends Figure
    {     

        const SIDES_COUNT = 4;
        
        private $a;
       
        function __construct($a) 
        { 
            $this->a = $a;   
        } 
        
        public function getArea()
        {
            $this->area = $this->a * $this->a;
            return $this->area;
        } 
        
        public final function infoAbout()
        {
            return "Это квадрат. У него 4 стороны";
        }

    }

    class Triangle extends Figure
    {    

        const SIDES_COUNT = 3;
        
        private $a;
        private $b;
        private $c;
        
        function __construct($a, $b, $c) 
        { 
            $this->a = $a;
            $this->b = $b;       
            $this->c = $c;       
        } 
        
        public function getArea()
        {
            $p = ( $this->a + $this->b + $this->c ) / 2;
            $this->area = sqrt( $p * ( $p - $this->a ) * ( $p - $this->b ) * ( $p - $this->c ) );
            return $this->area;
        } 
        
        public final function infoAbout()
        {
            return "Это треугольник. У него 3 стороны";
        }

    }

    $rectangle1 = new Rectangle(1, 2);
    $rectangle2 = new Rectangle(3, 6);

    $triangle1 = new Triangle(2, 2, 3);
    $triangle2 = new Triangle(5, 5, 9);

    $square1 = new Square(5);
    $square2 = new Square(10);

    $mass = [
        new Rectangle(1, 2),
        new Rectangle(3, 6),
        new Triangle(2, 2, 3),
        new Triangle(5, 5, 9),
        new Square(5),
        new Square(10)
    ];

    foreach( $mass as $key => $value ) {
        echo "<p>{$mass[$key]->infoAbout()} </p>";
        echo "<p>Площадь = {$mass[$key]->getArea()} </p><br/>";
    }

//Урок 15: Объектно-ориентированное программирование #3

/*В таблице books хранятся записи о книгах. Задача - получить эти записи и вывести так, как показано на картинке. 
Нужно получить примерно такой html код: 

<p>
    <img src="/images/BookPdf.jpg" alt="" />
    <a href="/files/webappsdev.pdf">Люк Веллинг, Разработка веб приложений</a>
</p>
<p>
    <img src="/images/BookDoc.jpg" alt="" />
    <a href="/files/phpmysql.doc">Кевин Янк, PHP MySQL</a>
</p>
… 

Подсказки и особенности: 
1. Решить задачу нужно, используя полиморфизм. 
2. Виды книг отличаются иконками. 
3. Составляйте план решения задачи:
    • определите логические части программы 
    • создайте файловую структуру 
    • реализуйте необходимые классы, методы*/

    echo "<h1>"."Lesson 15"."</h1>";

    class Book 
    {
        public $name;
        public $type;
        public $href;

        public function __construct($name, $type, $href) 
        {
            $this->name = $name;
            $this->type = $type;
            $this->href = $href;
        }

    }

    class BookPDF extends Book 
    {

        public $iconLink = "http://melissaormond.com/images/pdf.png";

    }

    class BookDOC extends Book 
    {

        public $iconLink = "http://g1zawiercie.pl/archiwum/1011/10-10-29-antygona/album/res/doc.gif";

    }

    class BookTXT extends Book 
    {

        public $iconLink = "http://www.fontsaddict.com/images/icons/png/thumb/8867.png";

    }

    class Library 
    {

        private $host = "localhost";
        private $user = "root";
        private $password = "";
        private $db = "book_base";
        private $connection;
        public $library = [];

        public function __construct() 
        {
            $this->connection = new mysqli($this->host, $this->user, $this->password, $this->db);
                
            if(mysqli_connect_error()) {
                trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(), E_USER_ERROR);
            }

            $result = mysqli_query($this->connection, 'SELECT * FROM books');

            while ( $row = mysqli_fetch_assoc($result) ) {                
                if ( $row['type'] == 'pdf' ) {
                    array_push($this->library, new BookPDF($row['name'], $row['type'], $row['href']) );
                } else if ( $row['type'] == 'doc' ) {
                    array_push($this->library, new BookDOC($row['name'], $row['type'], $row['href']) );
                } else if ( $row['type'] == 'txt' ) {
                    array_push($this->library, new BookTXT($row['name'], $row['type'], $row['href']) );
                } else {
                    echo "Неверный тип книги!";
                }
            }
            mysqli_close($this->connection);
        }

    }

    $lib = new Library();
    foreach ( $lib->library as $key => $value) {

        if ( $value->type == 'pdf' ) {
            echo '<p><img src="' . $value->iconLink . '" alt=""/><a href="' . $value->href . '">' . $value->name . '</a></p>';
        } else if ($value->type == 'doc') {
            echo '<p><img src="' . $value->iconLink . '" alt=""/><a href="' . $value->href . '">' . $value->name . '</a></p>';
        } else {
            echo '<p><img src="' . $value->iconLink . '" alt=""/><a href="' . $value->href . '">' . $value->name . '</a></p>';
        }
    }

?>
