<?php
//Урок 10: Сессии

/*Задача 1: Многостраничный тест 
Представим себе сайт с прохождением теста: на каждой странице находится вопрос, варианты ответа и кнопка “Далее”. Тест содержит некоторое количество страниц (допустим 3). На последней, 4-й странице пользователь должен получить результат теста. 
Задача – создать подобный тест. 
Подсказки: 
• Для создания страниц вопросов используйте формы (с элементами type=”radio”), которые будут отправлять результаты на следующую страницу с вопросом или результатами. 
• Ответы пользователя на вопросы на предыдущих страницах можно (и нужно) сохранять в сессии 
• Для расчета результатов теста сравните ответы пользователя (хранятся в сессии) с заранее определенными правильными ответами (могут храниться, например, в массиве или переменных). 
• После окончания текста сессию следует очистить*/

    session_start();

    if ( $_GET['smoke'] ) {
        $_SESSION['smoke'] = $_GET['smoke'];
    }

    if ($_SESSION['age'] && $_SESSION['sex'] && $_SESSION['smoke']) {
        
        if ( $_SESSION['age'] == 'age1' ) {
            $age = '1-20';
        } else {
            if ( $_SESSION['age'] == 'age2' ) {
                $age = '21-40'; 
            } else {
                $age = '41-60';  
            }
        }

        if ( $_SESSION['sex'] == 'male' ) {
            $sex = 'male';
        } else {
            $sex = 'female';  
        }

        if ( $_SESSION['smoke'] == 'smokeyes' ) {
            $smoke = 'yes';
        } else {
            $smoke = 'no';  
        }

        $true_answer = 'smokeno';

        if ( $_SESSION['smoke'] == $true_answer ) {
                $result = 'Good';
            } else {
                $result = 'Bad';  
            }
        
    }  

    session_unset();
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Lesson 10</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h2>Answer's</h2>
        <p>Age: <?=$age?><br></p>
        <p>Sex: <?=$sex?><br></p>
        <p>Smoke: <?=$smoke?><br></p>
        <h2>Result: <?=$result?></h2>
    </body>
</html>
