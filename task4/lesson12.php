<?php

//Урок 12: Базы данных #2
    
    $host = "localhost";
    $user = "root";
    $password = "";
    $db = "php_lab_bd";
    $con = mysql_connect($host, $user, $password);

    if ( !mysql_connect($host, $user, $password) ) {
        echo "<h2>MySQL Error!</h2>";
        exit;
    }

    mysql_select_db($db);

/*Задача 1: INSERT 
Добавить 3 записи в таблицу user с любыми данными одним запросом INSERT.*/

    $ins = " INSERT INTO `user` (`f_name`, `l_name`, `login`, `password`) 
             VALUES ('Дуглас', 'Борн', 'dborn', '12323qwerty'), 
                    ('Виктор', 'Рено', 'vreno', 'qwe123'), 
                    ('Марк', 'Николас', 'mnikolas', '345678')";

    //$ins_result = mysql_query($ins, $con);

/*Задача 2: DELETE; AND и OR в условиях WHERE
1) Написать запрос для удаления пользователей (таблица user) с именем "Томас". 
2) Написать запрос для удаления пользователей с именем "Томас" и фамилией "Смит". 
3) Написать запрос для удаления пользователей с именем "Томас" или именем "Джон".*/

    $del_1 = " DELETE FROM `user` WHERE `user`.`f_name` = 'Томас' ";
    $del_2 = " DELETE FROM `user` WHERE `user`.`f_name` = 'Томас' and `user`.`l_name` = 'Смит' ";
    $del_3 = " DELETE FROM `user` WHERE `user`.`f_name` = 'Томас' or `user`.`f_name` = 'Джон' ";

    //$del_1_result = mysql_query($del_1, $con);
    //$del_2_result = mysql_query($del_2, $con);
    //$del_3_result = mysql_query($del_3, $con);

/*Задача 3: UPDATE
1) Изменить имя (на "Тимофей") и фамилию (на "Опель") пользователю с идентификатором "3". 
2) Изменить фамилию троим пользователям с найбольшими значениями id на "Смит".*/

    $upd_1 = " UPDATE `user` SET `user`.`f_name` = 'Тимофей', `user`.`l_name` = 'Опель' WHERE `user`.`id` = 3 ";
    $upd_2 = " UPDATE `user` SET `user`.`l_name` = 'Смит' ORDER BY `id` DESC LIMIT 3 ";

    //$upd_1_result = mysql_query($upd_1, $con);
    //$upd_2_result = mysql_query($upd_2, $con);

/*Задача 4: SELECT 
1) Выбрать 3 последних новости из категории с идентификатором 2. 
2) Выбрать всех пользователей с именем "Владислав" или "Елена". 
3) В категории новости подсчитать количество новостей со статусом "1" и "0".*/

    $sel_1 = " SELECT * FROM `news` WHERE `news`.`id_category` = 2 ORDER BY `id` DESC LIMIT 3 ";
    $sel_2 = " SELECT * FROM `user` WHERE `user`.`f_name` = 'Владислав' or `user`.`f_name` = 'Елена' ";
    $sel_3 = " SELECT COUNT(*) AS count FROM `news` WHERE `news`.`status` = 1 or `news`.`status` = 0 GROUP BY `news`.`status` ";

/*Задача 5: SELECT + PHP 
Вывести список всех новостей со статусом "1" в виде списка: 
<h1>Заголовок</h1>
<p>Короткое описание</p>
<a href=”/news/view/[id]”>Читать далее </a>*/

    $res = " SELECT * FROM `news` WHERE `news`.`status` = 1 ";
    $res = mysql_query($res, $con);
    $count = mySQL_num_rows($res);

    for( $i = 0; $i < $count; $i++ ) {
        $f = mysql_fetch_array($res);
        echo '<h1>Новость №' . $f[id] . '</h1><br>';
        echo '<p>' . substr($f[text], 0, 300) . '...</p><br>' ;
        echo '<a href=”/news/view/[' . $f[id] . ']”>Читать далее </a><br>' ;
        echo '<p>Дата публикации: ' . $f[date] . '</p><br>';
    }

//Close connection
    
    mysql_close($con);

?>
