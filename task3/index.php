<?php

//Урок 6: Пользовательские функции

    echo "<h1>"."Lesson 6"."</h1>";

//Task 1

    echo "<h4>"."Task 1"."</h4>";

    $products = array(
        array('name' => 'Телевизор', 'price' => '400', 'quantity' => 1),
        array('name' => 'Телефон', 'price' => '300', 'quantity' => 3),
        array('name' => 'Кроссовки', 'price' => '150', 'quantity' => 2),
    );

    function basketInfo($products)
    {
        $summ = 0; 
        $count = 0;

        foreach($products as $val) {
            $summ += $val[price] * $val[quantity];
            $count += $val[quantity];
        }

        return $result = array(summ => $summ, count => $count);
    }

    $res = basketInfo($products);
    echo "Total summa = ".$res[summ].".<br>Total guantity = ".$res[count].".<br>";

//Task 2
    
    echo "<h4>"."Task 2"."</h4>";
    
    function quadraticEquation($a, $b, $c)
    {
        $d = $b*$b - 4*$a*$c;
        $result;

        if ( $d > 0 ) {
            $result = array("x(1)" => ( ( -$b + sqrt($d) ) / 2*$a ), "x(2)" => ( ( -$b - sqrt($d) ) / 2*$a ) );
        } elseif ( $d == 0) {
            $result = ( -$b / 2*$a );
        } else {
            $result = false;
        }

        return $result;
    }
    
    $res = quadraticEquation(2, 4, 1);
    if ( $res == false) {
        echo "No root";
    } else {
        print_r($res);
    }

//Task 3
    
    echo "<h4>"."Task 3"."</h4>"; 

    $digits = array(2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95);
    echo "Elements of digits:<br>";
    print_r($digits);

    function deleteNegativesV1($digits)
    {
        foreach($digits as $key => $val) {
            if ( $val < 0 ) {
                unset($digits[$key]);
            } 
        }
        return $digits;
    }

    $digits = deleteNegativesV1($digits);
    echo "<br><br>Elements of digits after deleteNegatives:<br>";
    print_r($digits);
    
//Task 4

    echo "<h4>"."Task 4"."</h4>"; 
    
    $digits = array(2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95);
    echo "Elements of digits:<br>";
    print_r($digits);

    function deleteNegativesV2(&$digits)
    {
        foreach($digits as $key => $val) {
            if ( $val < 0 ) {
                unset($digits[$key]);
            } 
        }
    }

    deleteNegativesV2($digits);
    echo "<br><br>Elements of digits after deleteNegatives:<br>";
    print_r($digits);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Lesson 6 and 8</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Lesson 8</h1>
        <h4>Task 2</h4>
        <div>
            <form action="/index.php" method="GET">
                Text 1: <input type="text" name="text1" value="" /> <br/>
                Text 2: <input type="text" name="text2" value="" /> <br/>
                Text 3: <input type="text" name="text3" value="" /> <br/>
                Text 4: <input type="text" name="text4" value="" /> <br/>
                Text 5: <input type="text" name="text5" value="" /> <br/>
                Text 6: <input type="text" name="text6" value="" /> <br/>
                Text 7: <input type="text" name="text7" value="" /> <br/>
                <br/>
                <input type="submit" value="Submit"/>
            </form>
        </div>
        <?php
        //Урок 8: HTTP, Формы
        //Task 2
            $formMass = array();
            $emptyFlag = true;

            foreach($_GET as $key => $val) {
                if ( !empty($val) ) {
                    $emptyFlag = false;
                    $formMass[$key] = intval($val);
                }
            } 
            
            if (!$emptyFlag) {
                $max = max($formMass);
                $min = min($formMass);
                $summass = 0;
                $countmass = count($formMass);

                foreach($formMass as $val) {
                    $summass += $val;
                }

                echo "MAX = ".$max."<br/>";
                echo "MIN = ".$min."<br/>";
                echo "Average = ".( $summass / $countmass );
            }
        ?>
        <h4>Task 3</h4>
        <div>
            <form action="/index.php" method="POST">
                Name: <input name="name" value="" /> <br/>
                <br/>
                Male: <input type="radio" name="sex" value="male" required/> <br/>
                Female: <input type="radio" name="sex" value="female" required/> <br/>
                <br/>
                <input type="submit" name="submit2" value="Submit" />
            </form>
        </div>
        <?php
        //Урок 8: HTTP, Формы
        //Task 3    
            if ( !empty($_POST[name]) ) {
                if ($_POST[sex] == "male") {
                    echo "Добро пожаловать, мистер ".$_POST[name]."!";
                } else {
                    echo "Добро пожаловать, миссис ".$_POST[name]."!";
                }
            }
        ?>
    </body>
</html>
