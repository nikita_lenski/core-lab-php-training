<?php

//Урок 3: Типы переменных

    echo "<h1>"."Lesson 3"."</h1>";
    
//Task 1
    
    echo "<h4>"."Task 1"."</h4>"; 

    $a = 152; 
    $b = '152'; 
    $c = 'London';
    $d = array(152); 
    $e = 15.2; 
    $f = false; 
    $g = true;
    
    echo "Value a - ". gettype($a)."<br>";
    echo "Value b - ". gettype($b)."<br>";
    echo "Value c - ". gettype($c)."<br>";
    echo "Value d - ". gettype($d)."<br>";
    echo "Value e - ". gettype($e)."<br>";
    echo "Value f - ". gettype($f)."<br>";
    echo "Value g - ". gettype($g)."<br>";
    
//Task 2
    
    echo "<h4>"."Task 2"."</h4>"; 
    
    $valA = 5;
    $valB = 10;  
    
    echo "Сегодня ".$valA." из ".$valB." студентов посетили лекцию.<br>";   
    echo 'Сегодня '.$valA.' из '.$valB.' студентов посетили лекцию.';

//Task 3
    
    echo "<h4>"."Task 3"."</h4>"; 
    
    $par1 = "Доброе утро";
    $par2 = "дамы"; 
    $par3 = "и господа";
    
    echo $par1."<br>";   
    echo $par2."<br>";  
    echo $par3."<br><br>";  
    echo $par1.', '.$par2.' '.$par3;
    
//Task 4

    echo "<h4>"."Task 4"."</h4>"; 
    
    $mas1 = array(1, 2, 3, 4, 5); 
    $mas2 = array(6, 7, 8, 9, 10); 
    
    $mas1["elemet"] = 11;
    
    unset($mas2[0]);
    
    echo "mas1[2] = ".$mas1[2]."<br>";
    
    echo "<br>Elements of mas1:<br>";
    foreach($mas1 as $val)
        echo "$val\n";
        
    echo "<br><br>Elements of mas2:<br>";
    for($i = 0; $i < count($mas2) + 1; $i++) {
        echo "$mas2[$i]\n";
    }

    echo "<br><br>Count(mas1) = ".sizeof($mas1)."<br>";
    echo "Count(mas2) = ".sizeof($mas2)."<br>";
    
//Урок 4: Условные операторы

    echo "<h1>"."Lesson 4"."</h1>";
    
//Task 1

    echo "<h4>"."Task 1"."</h4>"; 
    
    define("MIN", 10);
    define("MAX", 50);
    $num = 25;
        
    if ( $num == MIN || $num == MAX):
	echo "+-<br>";
    elseif ( $num > MIN and $num < MAX): 
        echo "+<br>";
    else:
        echo "-<br>";
    endif;
    
//Task 2
    
    echo "<h4>"."Task 2"."</h4>"; 
    
    $a = 2;
    $b = 4;
    $c = 2;
    
    $d = $b*$b - 4*$a*$c;
    
    if ( $d > 0 ): {        
	echo "x(1) = ".( ( -$b + sqrt($d) ) / 2*$a )."<br>";        
	echo "x(2) = ".( ( -$b - sqrt($d) ) / 2*$a )."<br>";
    }
    elseif ( $d == 0): 
        echo "x(1) = ".( -$b / 2*$a )."<br>";  
    else:
        echo "No roots<br>";
    endif;
    
//Task 3
    
    echo "<h4>"."Task 3"."</h4>"; 

    $aa = 1;
    $bb = 2;
    $cc = 3;
        
    if ( $aa == $$ $bb || $aa == $cc || $bb == $cc ) {
        echo "Error";        
    } else {
        if ( $aa > $bb && $aa > $cc): {
            if ( $bb > $cc) {
                echo "Среднее число: b = ".$bb;
            } else {
                echo "Среднее число: c = ".$cc;                
            }
        };
        elseif ( $bb > $aa && $bb > $cc): {
            if ( $aa > $cc) {
                echo "Среднее число: a = ".$aa;
            } else {
                echo "Среднее число: c = ".$cc;
            }
        };  
        else: {
            if ( $aa > $bb) {
                echo "Среднее число: a = ".$aa;
            } else {
                echo "Среднее число: b = ".$bb;                
            }                
        };            
        endif;  
    }
    
//Урок 5: Циклы

    echo "<h1>"."Lesson 5"."</h1>";
    
//Task 1

    echo "<h4>"."Task 1"."</h4>"; 
        
    $sum1 = 0;
    $sum2 = 0;
    
    for($i = 0; $i < 25; $i++) {
        $sum1 += $i;
    }
    
    $i = 0;
    while ($i < 25) {
        $sum2 += $i;
        $i++;
    }
    
    echo "SumFOR(1...25) = ".$sum1."<br>";
    echo "SumWHILE(1...25) = ".$sum2;
    
//Task 2
    
    echo "<h4>"."Task 2"."</h4>"; 
        
    $n = 150;
    
    echo "N = ".$n."<br><br>";
    
    for($i = 1; $i*$i <= $n; $i++) {
        echo $i."*".$i." = ".($i * $i)."<br>";
    }
        
//Task 3
    
    echo "<h4>"."Task 3"."</h4>"; 
    
    $mass = array();
    
    for($i = 10; $i > 0; $i--) {
        $mass[$i] = "Кнопка ".$i;
    }
    
    echo "Elements of mass:<br>";
    foreach($mass as $val)
        echo "$val\n";
    
    ksort($mass);
    
    echo "<br><br>Elements of mass after sort:<br>";
    foreach($mass as $val)
        echo "$val\n";
    
    echo "<ul><br>";    
    foreach($mass as $val)
        echo '<li><a href="#">'.$val."</a></li>";
    echo "<br></ul>"; 
      
?>

