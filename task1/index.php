﻿<?php
//Урок 2: Основы

    echo "<h1>"."Lesson 2"."</h1>";
    
//Task 1

    echo "<h4>"."Task 1"."</h4>"; 
    //Nikita Lenski, 01/11/17

    /*
     * Nikita Lenski
     * 01/11/17 
     */

    echo "Hello, this task was accomplished by Nikita Lenski, 01/11/17";

//Task 2
    
    $tvChannel = "AMC";
    $address = "Belarus, Gomel, R80";
    $colorCar = "red";
    $temperatureWater = 23;
    $modelPhone = "SG9500I";

//Task 3
    
    echo "<h4>"."Task 3"."</h4>"; 
    
    $par1 = 3;
    $par2 = 5;
    $par3 = 8;
    $sum = $par1 + $par2 + $par3;
    $result = 2 + 6 + 2/5 + 1;
    
    echo $par1." ".$par2." ".$par3."<br>";    
    echo "Sum = ".$sum."<br>";
    echo "Result = ".$result."<br>";
    
//Task 4

    echo "<h4>"."Task 4"."</h4>"; 
    
    $a = 1;
    $b = 2;
    $c = $a; 
    $d = &$b;
       
    echo "a = ".$a."<br>";
    echo "b = ".$b."<br>";
    echo "c = ".$c."<br>";
    echo "d = ".$d."<br><br>";
    
    $a = 3;
    $b = 4;

    echo "a = ".$a."<br>";
    echo "b = ".$b."<br>";
    echo "c = ".$c."<br>";
    echo "d = ".$d."<br>";    
    
//Task 5
    
    echo "<h4>"."Task 5"."</h4>"; 
    
    define("CONST1", 41);
    define("CONST2", 33);
    
    echo "Sum(CONST1, CONST2) = ".(CONST1 + CONST2)."<br>";
    
    define("CONST1", 5);
    $CONST1 = 5;
    
?>

